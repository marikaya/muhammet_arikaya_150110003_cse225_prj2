#include "dataproject.h"


Album * header;
unsigned int choise;
int main(void)
{
    /*header = AddAlbum(header,"album1",2009,"yasar");
    header = AddAlbum(header,"album2",8,"yasar");
    AddSongToAlbum(header,"album1","sarki1",1);
    AddSongToAlbum(header,"album1","sarki2",10);
    AddSongToAlbum(header,"album1","sarki3",100);
    AddSongToAlbum(header,"album2","sarki1",1);
    AddSongToAlbum(header,"album2","sarki2",10);
    AddSongToAlbum(header,"album2","sarki3",100);
    ShowAlbum(header,"album1"); */
   
    do{       
        printf("1. Add an album\n");
        printf("2. Remove an album\n");
        printf("3. Show the list of albums\n");
        printf("4. Show detailed information about a particular album\n");
        printf("5. Add a song to the song list of an album\n");
        printf("6. Remove a song from the song list of a album\n");
        printf("7. Show the list of all songs\n");
        printf("8. Query the albums which are/were released between particular years\n");
        printf("9. Query the songs whose lengths are in a particular scope\n");
        printf("0. Exit Program \n");
        choise = UTIL_getInt("Please select choise : ");
        printf("==============================================================\n");       
        switch(choise)
        {
            case 1:
            User_AddAlbum();
            break;
            case 2:
            User_RemoveAlbum();
            break;
            case 3:
            User_ListAlbum();
            break;
            case 4:
            User_ShowAlbum();
            break;
            case 5:
            User_AddSong();
            break;
            case 6:
            User_RemoveSong();
            break;
            case 7:
            User_ListAllSongs();
            break;
            case 8:
            User_GetAlbumByYear();
            break;
            case 9:
            User_GetSongByLength();
            break;
        }  
        
    }while(choise != 9);    
    return 0;
}
void User_AddAlbum(){
    char * albumTitle = UTIL_getString("Enter Album Title : ");
    char * albumSinger = UTIL_getString("Enter Album Singer : ");
    unsigned int albumYear = UTIL_getInt("Enter Album Year : ");
    
    if ( albumYear < 1700 || albumYear > 2014){
        printf("you must provide valid information\n");
        return;
    }
    header = AddAlbum(header,albumTitle,albumYear,albumSinger); 
    
}
void User_RemoveAlbum(){
    char * albumTitle = UTIL_getString("Enter Album Title : ");    
    header = RemoveAlbum(header,albumTitle);
}
void User_ListAlbum(){
    ShowAlbums(header);    
}
void User_ShowAlbum(){
    char * albumTitle = UTIL_getString("Enter Album Title : ");
    ShowAlbum(header,albumTitle);
}
void User_AddSong(){
    char * albumTitle = UTIL_getString("Enter Album Title : ");
    char * songName = UTIL_getString("Enter Song Name : ");
    unsigned int songLength = UTIL_getInt("Enter Song Length : ");
    if ( songLength < 0 ){
        printf("you must provide valid information\n\r");
        return;
    }
    AddSongToAlbum(header,albumTitle,songName,songLength);   
}
void User_RemoveSong(){
    char * albumTitle = UTIL_getString("Enter Album Title : ");
    char * songName = UTIL_getString("Enter Song Name : ");
    
    RemoveSongFromAlbum(header,albumTitle,songName);
}
void User_ListAllSongs(){
    TraverseAlbums(header);
}
void User_GetAlbumByYear(){
    unsigned int lowerBoundary = UTIL_getInt("Lower Boundary : ");
    unsigned int upperBoundary = UTIL_getInt("Upper Boundary : ");
    if ( lowerBoundary < 0 || upperBoundary < 0 ){
        printf("you must provide valid information\n\r");
        return;
    }
    if ( lowerBoundary >= upperBoundary){
        printf("you must provide valid information\n");
        return;
    }
    ShowByYearFilter(header,lowerBoundary,upperBoundary);
}
void User_GetSongByLength(){   
    unsigned int lowerLength = UTIL_getInt("Lower Length : ");
    unsigned int upperLength = UTIL_getInt("Upper Length : ");
    if ( lowerLength < 0 || upperLength < 0 ){
        printf("you must provide valid information\n\r");
        return;
    }
    if ( lowerLength >= upperLength){
        printf("you must provide valid information\n\r");
        return;
    }
    TraverseAlbumsForSongFilter(header,lowerLength,upperLength);
}





































