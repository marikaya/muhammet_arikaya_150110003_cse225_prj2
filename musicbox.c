#include "musicbox.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


Album * AddAlbum(Album * header,char * title,unsigned int releaseYear,char * singerName){
    if ( header == NULL){
        Album * temp = (Album*)malloc(sizeof(Album));
        temp->title = title;
        temp->releaseYear = releaseYear;
        temp->singerName = singerName;
        temp->songs = NULL;
        temp->left = NULL;
        temp->right = NULL;
        return temp;
    }
    if ( strcmp(header->title,title) == 0 ){
        printf("Album already added before\n");        
        return header;
    }
    if ( strcmp(title,header->title) > 0 ){
        header->right = AddAlbum(header->right,title,releaseYear,singerName);
    }
    if ( strcmp(title,header->title) < 0 ){
        header->left = AddAlbum(header->left,title,releaseYear,singerName);
    }    
    return header;
}
Album * FindMinAlbum(Album *node)
{
        if(node==NULL){
                printf("There is no element in tree\n");
                return NULL;
        }
        if(node->left) 
                return FindMinAlbum(node->left);
        else 
                return node;
}
Album* FindMaxAlbum(Album *node)
{
        if(node==NULL){
            printf("There is no element in tree\n");
            return NULL;
        }
        if(node->right) 
                FindMaxAlbum(node->right);
        else 
                return node;
        return 0;
}
Album * RemoveAlbum(Album *node, char * title)
{
        Album *temp;
        if(node==NULL){               
            printf("Element Not Found.\n");
        }
        else if(strcmp(title,node->title) < 0 ){           
                node->left = RemoveAlbum(node->left, title);
        }
        else if(strcmp(title,node->title) > 0 ){            
                node->right = RemoveAlbum(node->right, title);
        }
        else{ 
             
                if(node->right && node->left){                      
                        temp = FindMinAlbum(node->right);
                        node -> title = temp->title;                       
                        node -> right = RemoveAlbum(node->right,temp->title);
                }
                else{                       
                        temp = node;
                        if(node->left == NULL)
                                node = node->right;
                        else if(node->right == NULL)
                                node = node->left;
                        free(temp); /* temp is longer required */ 
                }
        }
        return node;
}
Album * FindAlbum(Album *node, char * title)
{
        if( node == NULL){
            printf("Album was not found\n");
            return NULL;
        }
        if(strcmp(title,node->title) > 0 ){              
                return FindAlbum(node->right,title);
        }
        else if(strcmp(title,node->title) < 0 ){    
                return FindAlbum(node->left,title);
        }
        else{               
                return node;
        }
}

void ShowAlbums(Album *header){
    if ( header == NULL){
        return;
    }
    ShowAlbums(header->left);    
    printf("AlbumTitle [%s] AlbumYear [%d] AlbumSinger [%s]\n",header->title,header->releaseYear,header->singerName);
    ShowAlbums(header->right);    
}
void ShowSongs(Song *header){
    if ( header == NULL){
        return;
    }
    ShowSongs(header->left);    
    printf("SongTitle [%s] Song Length [%d] \n",header->name,header->length);   
    ShowSongs(header->right);
}

void ShowAlbum(Album * header,char * title){
    if ( header == NULL){
        printf("Album is empty\n");
        return;
    }
    Album * tempAlbum;
    
    tempAlbum = FindAlbum(header,title);
    
    if ( tempAlbum == NULL){
        printf("Album was not found\n");
        return;
    }
    
    printf("Album Title [%s] Album Singer[%s] Album Year[%d]\n",tempAlbum->title,tempAlbum->singerName,tempAlbum->releaseYear);
    ShowSongs(tempAlbum->songs);
}
Song* FindMinSong(Song *node)
{
        if(node==NULL){               
                return NULL;
        }
        if(node->left)
                return FindMinSong(node->left);
        else 
                return node;
}
Song* FindMaxSong(Song *node)
{
        if(node==NULL){               
                return NULL;
        }
        if(node->right)
                FindMaxSong(node->right);
        else 
                return node;
}

Song * AddSong(Song *node,char * songName,unsigned int songLength)
{
        if(node==NULL){
                Song *temp;
                temp = (Song *)malloc(sizeof(Song));
                temp ->name = songName;
                temp->length = songLength;
                temp -> left = temp -> right = NULL;
                return temp;
        }
        if(strcmp(songName,node->name) > 0){
                node->right = AddSong(node->right,songName,songLength);
        }
        else if(strcmp(songName,node->name) < 0){
                node->left = AddSong(node->left,songName,songLength);
        }
        return node;
}

Song * RemoveSong(Song *node,char * songName)
{       
        Song *temp;
        if( node == NULL){
                printf("Song Not Found\n");
        }
        else if(strcmp(songName,node->name) < 0 ){
                node->left = RemoveSong(node->left, songName);
        }
        else if(strcmp(songName,node->name) > 0 ){
                node->right = RemoveSong(node->right, songName);
        }
        else{
                if(node->right && node->left){                      
                        temp = FindMinSong(node->right);
                        node -> name = temp->name;                     
                        node -> right = RemoveSong(node->right,temp->name);
                }
                else{                      
                        temp = node;
                        if(node->left == NULL)
                                node = node->right;
                        else if(node->right == NULL)
                                node = node->left;
                        free(temp);
                }
        }
        return node;

}

Song * FindSong(Song *node, char * songName)
{
        if( node == NULL ){               
                return NULL;
        }
        if(strcmp(songName,node->name) > 0){              
                return FindSong(node->right,songName);
        }
        else if(strcmp(songName,node->name) < 0){
                return FindSong(node->left,songName);
        }
        else{                
                return node;
        }
}
void * RemoveSongFromAlbum(Album * node,char * albumTitle,char * songName){
    Album * tempAlbum;
    Song * tempSong;
    tempAlbum = FindAlbum(node,albumTitle);
    if (tempAlbum == NULL){
        printf("Album was not found\n");
        return;       
    }else{
        tempSong = FindSong(tempAlbum->songs,songName);
        if ( tempSong != NULL){            
             tempAlbum->songs = RemoveSong(tempAlbum->songs,songName);
        }else{           
            printf("Song Not Found\n");
            return node;
        }
    }    
}
void * AddSongToAlbum(Album * node,char * albumTitle,char *  songName,unsigned int songLength ){    
    Album * tempAlbum;
    Song * tempSong;
    
    tempAlbum = FindAlbum(node,albumTitle);
    if (tempAlbum == NULL){
        printf("Album was not found\n");
        return;       
    }else{
        tempSong = FindSong(tempAlbum->songs,songName);
        if ( tempSong != NULL){ 
            printf("Song Already Added\n");
             return tempAlbum;
        }else{
            tempAlbum->songs = AddSong(tempAlbum->songs,songName,songLength);
            printf("Album added\n");
            return node;
        }
    }
}
void TraverseAlbums(Album * node){
    if ( node == NULL){
        return;
    }
    TraverseSongs(node->songs);
    TraverseAlbums(node->left);
    TraverseAlbums(node->right);
}

void TraverseSongs(Song * node){
    if ( node == NULL){
        return;
    }
    printf("Song Name [%s]",node->name);
    TraverseSongs(node->left);
    TraverseSongs(node->right);
}

void ShowByYearFilter(Album * node,unsigned int lower,unsigned int upper){    
    if ( node == NULL){
        return;
    }
    if ( node->releaseYear > lower  && node->releaseYear < upper){
        printf("AlbumTitle [%s], Release Year[%d]",node->title,node->releaseYear);
    }
    ShowByYearFilter(node->left,lower,upper);
    ShowByYearFilter(node->right,lower,upper);
}

void TraverseAlbumsForSongFilter(Album * node,unsigned int lower,unsigned int upper){
    if ( node == NULL){
        return;
    }
    ShowByLengthFilter(node->songs,lower,upper);
    TraverseAlbumsForSongFilter(node->left,lower,upper);
    TraverseAlbumsForSongFilter(node->right,lower,upper);
}
void ShowByLengthFilter(Song * node,unsigned int lower,unsigned int upper){
    if ( node == NULL){
        return;
    }
    if ( node->length > lower && node->length < upper){
        printf("SongName [%s], Song Length[%d]\n",node->name,node->length);
    }
    ShowByLengthFilter(node->left,lower,upper);
    ShowByLengthFilter(node->right,lower,upper);
}
