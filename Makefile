CC=gcc
DEPS = util.h dataproject.h musicbox.h
OBJ = util.o dataproject.o musicbox.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $<

dataproject: $(OBJ)
	gcc -o $@ $^
clean:
	rm -f *.o
