#ifndef DATAPROJECT_H
#define DATAPROJECT_H

#include <stdio.h>
#include "util.h"
#include "musicbox.h"


void User_AddAlbum();
void User_RemoveAlbum();
void User_ListAlbum();
void User_ShowAlbum();
void User_AddSong();
void User_RemoveSong();
void User_ListAllSongs();
void User_GetAlbumByYear();
void User_GetSongByLength();

#endif // DATAPROJECT_H
