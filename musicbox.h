#ifndef MUSICBOX_H
#define MUSICBOX_H


typedef struct SongNode{
        char * name;
        unsigned int length;
        struct SongNode * left;
        struct SongNode * right;
}Song;

typedef struct AlbumNode{
    char * title;
    unsigned int releaseYear;
    char * singerName;
    Song * songs;
    struct AlbumNode * left;
    struct AlbumNode * right;
}Album;

Album * AddAlbum(Album * ,char * ,unsigned int ,char * );
Album * RemoveAlbum(Album *, char * );
Album * FindAlbum(Album *, char * );
Song * FindSong(Song *, char * );
Album* FindMaxAlbum(Album *);
Album * FindMinAlbum(Album *);


void ShowAlbums(Album * );
void ShowSongs(Song *);
void ShowAlbum(Album * ,char *);
Song * AddSong(Song *node,char * songName,unsigned int songLength);



void * RemoveSongFromAlbum(Album * node,char * albumTitle,char * songName);
void * AddSongToAlbum(Album * node,char * albumTitle,char *  songName,unsigned int songLength );

void TraverseAlbums(Album * node);
void TraverseSongs(Song * node);

void ShowByYearFilter(Album * node,unsigned int lower,unsigned int upper);
void TraverseAlbumsForSongFilter(Album * header,unsigned int lower,unsigned int upper);






#endif // MUSICBOX_H
