#ifndef UTIL_H
#define UTIL_H
#include <stdio.h>
#include <stdlib.h>


unsigned int UTIL_getInt(char *);
char * UTIL_getString(char * );
void Console_Log(int type,char *);
#endif // UTIL_H
