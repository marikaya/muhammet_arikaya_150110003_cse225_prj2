TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    util.c \
    musicbox.c \
    dataproject.c

HEADERS += \
    util.h \
    musicbox.h \
    dataproject.h

